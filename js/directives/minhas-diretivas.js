angular.module('minhasDiretivas', [])
	.directive('meuPainel', function() {

		var ddo = {};

		ddo.restrict = "AE";
        ddo.transclude = true;


		ddo.scope = {
            nome: '@'
        };

        ddo.templateUrl = 'js/directives/meu-painel.html';

		return ddo;
	})
    
    .directive('meuBotaoPerigo', function() {
        var ddo = {};
        ddo.restrict = "E";
        ddo.scope = {
            nome: '@',
            acao : '&'
        }
        ddo.template = '<button class="btn btn-danger btn-block" ng-click="acao()">{{nome}}</button>';

        return ddo;
    })
    .directive('meuFocus', function() {
        var ddo = {};
        ddo.restrict = "A";
       
        ddo.link = function(scope, element) {
             scope.$on('usuarioCadastrado', function() {
                 element[0].focus();
             });
        };

        return ddo;
    })
    .directive('meusTitulos', function() {
        var ddo = {};
        ddo.restrict = 'E';
        ddo.template = '<ul><li ng-repeat="nome in nomes">{{nome}}</li></ul>';
        ddo.controller = function($scope, recursoUsuario) {
            recursoUsuario.query(function(usuarios) {
                $scope.nomes = usuarios.map(function(usuario) {
                    return usuario.nome;
                });    
            });
        };
        return ddo;
    });
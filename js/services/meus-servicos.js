angular.module('meusServicos', ['ngResource'])
	.factory('recursoUsuario', function($resource) {

		return $resource('http://localhost:8080/CiagREST/services/usuarios:usuarioId', null, {
			'update' : { 
				method: 'PUT'
			}
		});
	})
	.factory("cadastroDeUsuarios", function(recursoUsuario, $q, $rootScope) {
		
		var evento = 'usuarioCadastrado';

		var service = {};

		service.cadastrar = function(usuario) {
			return $q(function(resolve, reject) {

				if(usuario.id) {
					recursoUsuario.update({usuarioId: usuario.id}, usuario, function() {

						$rootScope.$broadcast(evento);
						resolve({
							mensagem: 'Usuario ' + usuario.nome + ' atualizado com sucesso',
							inclusao: false
						});
					}, function(erro) {
						console.log(erro);
						reject({
							mensagem: 'Não foi possível atualizar o usuario ' + usuario.nome
						});
					});

				} else {
					recursoUsuario.save(usuario, function() {
						$rootScope.$broadcast(evento);
						resolve({
							mensagem: 'Usuario ' + usuario.nome + ' incluído com sucesso',
							inclusao: true
						});
					}, function(erro) {
						console.log(erro);
						reject({
							mensagem: 'Não foi possível incluir a usuario ' + usuario.nome
						});
					});
				}
			});
		};
		return service;
    });
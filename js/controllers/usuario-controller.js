angular.module('ciag')
	.controller('UsuarioController', ['$scope', 'recursoUsuario', '$routeParams', 'cadastroDeUsuarios', function($scope, recursoUsuario, $routeParams, cadastroDeUsuarios) {

		$scope.usuario = {};
		$scope.mensagem = '';

		if($routeParams.id) {
			recursoUsuario.get({id: $routeParams.id}, function(usuario) {
				$scope.usuario = usuario; 
			}, function(erro) {
				console.log(erro);
				$scope.mensagem = 'Não foi possível obter a usuario'
			});
		}

		$scope.submeter = function() {

			if ($scope.formulario.$valid) {
				cadastroDeUsuarios.cadastrar($scope.usuario)
				.then(function(dados) {
					$scope.mensagem = dados.mensagem;
					if (dados.inclusao) $scope.usuario = {};
				})
				.catch(function(erro) {
					$scope.mensagem = erro.mensagem;
				});
			}
		};
	}]);
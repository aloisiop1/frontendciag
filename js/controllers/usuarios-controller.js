angular.module('ciag').controller('UsuariosController', function($scope, recursoUsuario) {
	
	$scope.usuarios = [];
	$scope.filtro = '';
	$scope.mensagem = '';

	recursoUsuario.query(function(usuarios) {
		$scope.usuarios = usuarios;
	}, function(erro) {
		console.log(erro);
	});

	$scope.remover = function(usuario) {

		recursoUsuario.delete({id: usuario.id}, function() {
			var indiceDoUsuario = $scope.usuarios.indexOf(usuario);
			$scope.usuarios.splice(indiceDoUsuario, 1);
			$scope.mensagem = 'Usuario ' + usuario.nome + ' removido com sucesso!';
		}, function(erro) {
			console.log(erro);
			$scope.mensagem = 'Não foi possível apagar a usuario ' + usuario.nome;
		});
	};

});
angular.module('ciag', ['minhasDiretivas','ngAnimate', 'ngRoute', 'ngResource', 'meusServicos'])
	.config(function($routeProvider, $locationProvider) {

		$locationProvider.html5Mode(true);

		$routeProvider.when('/usuarios', {
			templateUrl: 'partials/principal.html',
			controller: 'UsuariosController'
		});

		$routeProvider.when('/usuarios/new', {
			templateUrl: 'partials/usuario.html',
			controller: 'UsuarioController'
		});

		$routeProvider.when('/usuarios/edit/:id', {
			templateUrl: 'partials/usuario.html',
			controller: 'UsuarioController'
		});

		$routeProvider.otherwise({redirectTo: '/usuarios'});

	});